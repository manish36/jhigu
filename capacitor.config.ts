import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.jhigu.jhiguapp',
  appName: 'devdacticLogin',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
